/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function personDetails(){
		let fullName = prompt("Enter your Full Name: "),
			age = prompt("Enter your Age: "),
			city = prompt("Enter your City: ")

			console.log("Hello, " +fullName);
			console.log("You are " + age + " years old.");
			console.log("You live in " + city + " City");
	};

	personDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function myTopFiveBands(){
		console.log("1. Panic at The Disco!");
		console.log("2. Paramore");
		console.log("3. The Script");
		console.log("4. Twenty One Pilots");
		console.log("5. Ciggarettes after Sex");
	}

	myTopFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function myTopFIveMovies(){
		console.log("1. Your Name");
		console.log("Rotten Tomatoes Rating: 98%");
		console.log("2. Spirited Away");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("3. Jujutsu Kaisen 0");
		console.log("Rotten Tomatoes Rating: 98%");
		console.log("4. My Neighbor Totoro");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("5. House Moving Castle");
		console.log("Rotten Tomatoes Rating: 93%");
	}

	myTopFIveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

 printFriends();

// console.log(friend1);
// console.log(friend2);
